@extends('layouts.admin')

@section('content')
@if(!isset($registro->id))
    {!! Form::open(['url' => $caminho, 'files'=>true ]) !!}
@else
    {!! Form::model($registro, ['url' => $caminho.$registro->id, 'method'=>'put', 'files'=>true]) !!}
@endif
    {!! Form::hidden('id', null) !!}
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4 class="title">{{ $registro->nome or $titulo }} <span class="pull-right">{!! $html_toggle or '' !!}</span></h4>
            </div>
            <div class="content">
                    <div class="form-group" >
                        <div class="row">
                            <div class="col-sm-6" >
                                {!! Form::label('nome', 'Nome da Categoria:') !!}
                                {!! Form::text('nome', null, ['class' => 'form-control', 'autofocus required'] ) !!}
                            </div>

                        </div>
                    </div>

                    <div class="form-group" >
                        {!! Form::label('peso', 'Peso:') !!}
                     {!! Form::number('peso', null, ['class' => 'form-control'] ) !!}
                    </div>

                    @if( (isset($registro->imagem)) && $registro->imagem > 0)
                    <div class="form-group" >
                        <img src="{{route('getfile', array($registro->imagem, 'grande') )}}" alt="{{ $registro->arquivo->alt }}" class="img-responsive" />
                        <label><input type="checkbox" name="remover_arquivo[]" value="imagem" > Remover arquivo</label>
                    </div>
                    @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                @include('admin._partes._botao_voltar')
                {!! Form::submit('Gravar', ['class' => 'btn btn-fill btn-wd btn-success pull-right']) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection