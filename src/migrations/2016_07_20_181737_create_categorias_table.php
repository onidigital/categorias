<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriasTable extends Migration {

	public function up()
	{
		Schema::create('categorias', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->integer('peso');
			$table->tinyInteger('status');			
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('categorias');
	}
}