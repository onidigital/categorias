<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
    // Menu do slide:
    Route::resource('categorias', 'Onicmspack\Categorias\CategoriasController');
    Route::post('categorias/{id}/atualizar_status','Onicmspack\Categorias\CategoriasController@atualizar_status'); // Atualizar Status Ajax
});