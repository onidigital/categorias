<?php

namespace Onicmspack\Categorias\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    protected $fillable = [
                           'nome',
                           'peso',
                           'status',
                   
    ];

    // Para retornar o arquivo do slide:
    public function arquivo()
    {
        return $this->belongsTo('Onicmspack\Arquivos\Models\Arquivo', 'imagem');
    }
}